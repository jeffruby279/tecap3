﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Capstone.Web.Models
{
    public class ParkWithVotes : Park
    {
        public int Votes { get; set; }
    }
}
