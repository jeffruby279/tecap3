﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Capstone.Web.Models
{
    public class ParkDetailModel
    {
        public Park park { get; set; }

        public List<Weather> weatherForecast {get; set;}

        public ParkDetailModel(Park park, List<Weather> weatherForecast)
        {
            this.park = park;
            this.weatherForecast = weatherForecast;
        }
    }
}
