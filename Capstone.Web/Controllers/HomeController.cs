﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Capstone.Web.Models;
using Capstone.Web.DAO;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http;

namespace Capstone.Web.Controllers
{
    public class HomeController : Controller
    {
        private IParkDAO parkDAO;
        private IWeatherDAO weatherDAO;
        private ISurveyResultDAO surveyDAO;

        public HomeController(IParkDAO parkDAO, IWeatherDAO weatherDAO, ISurveyResultDAO surveyDAO)
        {
            this.parkDAO = parkDAO;
            this.weatherDAO = weatherDAO;
            this.surveyDAO = surveyDAO;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var parks = parkDAO.GetAllParks();

            return View(parks);
        }

        [HttpGet]
        public IActionResult Detail(string id)
        {
            var park = parkDAO.GetOnePark(id);
            TempData["Temp_Pref"] = GetTemperaturePreference();
            var weatherList = weatherDAO.GetFiveDayForecast(park.ParkCode);
            var parkDetailObject = new ParkDetailModel(park, weatherList);

            return View(parkDetailObject);
        }

        [HttpGet]
        public IActionResult RedirectDetail(string id, string tempPref)
        {
            SaveTemperaturePreference(tempPref);

            return RedirectToAction("Detail", new { id });
        }

        private void SaveTemperaturePreference(string tempPref)
        {
            HttpContext.Session.SetString("Temp_Pref", tempPref);
        }

        private string GetTemperaturePreference()
        {
            string preference = null;
            if (HttpContext.Session.GetString("Temp_Pref") == null)
            {
                preference = "F";
                SaveTemperaturePreference(preference);
            }
            else
            {
                preference = HttpContext.Session.GetString("Temp_Pref");
            }

            return preference;
        }

        [HttpGet]
        public IActionResult Survey()
        {
            var parks = parkDAO.GetAllParks();
            List<SelectListItem> ParkOptions = new List<SelectListItem>();
            foreach (var park in parks)
            {
                ParkOptions.Add(new SelectListItem() { Value = park.ParkCode, Text = park.ParkName });
            }
            TempData["ParkOptions"] = ParkOptions;

            var survey = new SurveyResult();
            return View(survey);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Survey(SurveyResult survey)
        {
            if (!ModelState.IsValid || !surveyDAO.InsertSurvey(survey))
            {
                TempData["SurveyConfirmation"] = "Unable to submit survey.";
                var parks = parkDAO.GetAllParks();
                List<SelectListItem> ParkOptions = new List<SelectListItem>();
                foreach (var park in parks)
                {
                    ParkOptions.Add(new SelectListItem() { Value = park.ParkCode, Text = park.ParkName });
                }
                TempData["ParkOptions"] = ParkOptions;

                return View("Survey", survey);
            }

            TempData["SurveyConfirmation"] = "Survey successfully submitted.";
            TempData["favParkCode"] = survey.ParkCode;
            return RedirectToAction("SurveyResult");
        }

        [HttpGet]
        public IActionResult SurveyResult()
        {
            var parks = surveyDAO.GetLeadingParks();
            return View(parks);
        }



        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
