﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Capstone.Web.Models;
using Dapper;

namespace Capstone.Web.DAO
{
    public class SurveyResultSqlDAO : ISurveyResultDAO
    {
        private string connString;
        public SurveyResultSqlDAO(string connString)
        {
            this.connString = connString;
        }

        private const string RANKEDPARKSSQL = "select park.parkCode, park.parkName, count(*) as votes from survey_result join park on survey_result.parkCode = park.parkCode group by park.parkCode, park.parkName order by votes desc, park.parkName";
        private const string INSERTSURVEYSQL = @"insert into survey_result values (@parkCode, @emailAddress, @state, @activityLevel)";

        public bool InsertSurvey(SurveyResult survey)
        {
            try
            {
                using (var conn = new SqlConnection(connString))
                {
                    conn.Open();
                    string cmd = INSERTSURVEYSQL;
                    return conn.Execute(cmd, 
                        new { parkCode = survey.ParkCode,
                              emailAddress = survey.EmailAddress,
                              state = survey.State,
                              activityLevel = survey.ActivityLevel }) == 1;
                }

            } catch (SqlException e)
            {
                return false;
            }
        }

        public List<ParkWithVotes> GetLeadingParks()
        {
            List<ParkWithVotes> parks = new List<ParkWithVotes>();
            try
            {
                using (var conn = new SqlConnection(connString))
                {
                    conn.Open();
                    string cmd = RANKEDPARKSSQL;
                    parks = conn.Query<ParkWithVotes>(cmd).ToList();
                }

            } catch (SqlException e)
            {
                throw;
            }

            return parks;
        }
    }
}
