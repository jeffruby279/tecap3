﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Capstone.Web.Models;
using Dapper;

namespace Capstone.Web.DAO
{
    public class WeatherSqlDAO : IWeatherDAO
    {
        private string connstring;

        public WeatherSqlDAO(string connstring)
        {
            this.connstring = connstring;
        }

        public List<Weather> GetFiveDayForecast()
        {
            throw new NotImplementedException();
        }

        public List<Weather> GetFiveDayForecast(string parkCode)
        {
            var fiveDayForecast = new List<Weather>();

            try
            {
                using(SqlConnection connection = new SqlConnection(connstring))
                {
                    connection.Open();
                    string cmd = "SELECT * FROM weather WHERE parkCode = @parkCode ORDER BY fiveDayForecastValue;";
                    fiveDayForecast = connection.Query<Weather>(cmd, new { parkCode = parkCode}).ToList();
                }
            }
            catch(SqlException ex)
            {
                throw;
            }

            return fiveDayForecast;
        }
    }
}
