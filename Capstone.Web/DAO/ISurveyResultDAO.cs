﻿using Capstone.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Capstone.Web.DAO
{
    public interface ISurveyResultDAO
    {
        /// <summary>
        /// Returns the park object of the park currently with the most favorites. Returns an empty list if the db is empty.
        /// </summary>
        /// <returns></returns>
        List<ParkWithVotes> GetLeadingParks();

        /// <summary>
        /// Attempts to insert a survey into the survey table
        /// </summary>
        /// <param name="survey"></param>
        /// <returns>true if successful</returns>
        bool InsertSurvey(SurveyResult survey);

    }
}
