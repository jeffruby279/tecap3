﻿using Capstone.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Capstone.Web.DAO
{
    public interface IParkDAO
    {
        /// <summary>
        /// Returns a list of Park objects of all the parks in the DB. 
        /// If the DB is empty will return an empty list.
        /// </summary>
        /// <returns></returns>
        List<Park> GetAllParks();

        /// <summary>
        /// Returns the Park object corresponding to the parkCode.
        /// Will return null if no matching park is found.
        /// </summary>
        /// <returns></returns>
        Park GetOnePark(string parkCode);

        
    }
}
