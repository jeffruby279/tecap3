﻿using Capstone.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Capstone.Web.DAO
{
    public interface IWeatherDAO
    {
        /// <summary>
        /// Gets five days of weather from the database and returns a list of weather.
        /// Returns an empty list if no mathcing parkCode found.
        /// </summary>
        /// <returns></returns>
        List<Weather> GetFiveDayForecast();
        List<Weather> GetFiveDayForecast(string parkCode);
    }
}
