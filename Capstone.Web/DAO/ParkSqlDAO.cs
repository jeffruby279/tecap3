﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Capstone.Web.Models;
using Dapper;

namespace Capstone.Web.DAO
{
    public class ParkSqlDAO : IParkDAO
    {
        private string connString;
        public ParkSqlDAO(string connString)
        {
            this.connString = connString;
        }

        public List<Park> GetAllParks()
        {
            List<Park> parks;

            try
            {
                using (SqlConnection conn = new SqlConnection(connString))
                {
                    conn.Open();
                    string cmd = "select * from park order by parkName";
                    parks = conn.Query<Park>(cmd).ToList();
                }
            } catch (SqlException ex)
            {
                throw;
            }

            return parks;
        }

        public Park GetOnePark(string parkCode)
        {
            List<Park> parks;

            try
            {
                using (SqlConnection conn = new SqlConnection(connString))
                {
                    conn.Open();
                    string cmd = "SELECT * FROM park WHERE parkCode = @parkCode";
                    parks = conn.Query<Park>(cmd, new { parkCode = parkCode }).ToList();
                }
            }
            catch (SqlException ex)
            {
                return null; ;
            }
            return parks.FirstOrDefault();
        }
    }
}
