﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Transactions;
using System.Data.SqlClient;
using System;
using Capstone.Web.DAO;
using Dapper;
using Capstone.Web.Models;
using System.Collections.Generic;
using System.Linq;

namespace Capstone.Web.Tests.DAO
{
    [TestClass]
    public class WeatherSqlDAOTests
    {
        private string connString = Helper.CONNSTRING;
        private TransactionScope scope;
        WeatherSqlDAO weatherDAO;


        private List<Weather> forecast;

        [TestInitialize]
        public void Initialize()
        {
            scope = new TransactionScope();
            weatherDAO = new WeatherSqlDAO(connString);

            try
            {
                using (var conn = new SqlConnection(connString))
                {
                    conn.Open();
                    conn.Execute("insert into park values ('test', 'test', 'pa', 1, 1, 1, 1, 'test', 1, 1, 'test', 'test', 'test', 1, 1)");
                    conn.Execute("insert into weather values ('test', 1, 0, 100, 'ok')");
                }
            } catch (SqlException e)
            {
                throw;
            }
         
        }

        [TestCleanup]
        public void Cleanup()
        {
            scope.Dispose();
        }

        [TestMethod]
        public void GetForecastTest()
        {
            forecast = weatherDAO.GetFiveDayForecast("test");
            Assert.IsNotNull(forecast);
            CollectionAssert.AllItemsAreNotNull(forecast);
            Assert.AreEqual(1, forecast.Count);
            Assert.AreEqual(1, forecast[0].FiveDayForecastValue);
        }

        [TestMethod]
        public void GetForecastEmptyTest()
        {
            Assert.AreEqual(0, weatherDAO.GetFiveDayForecast("nosuch").Count);
        }
    }
}
