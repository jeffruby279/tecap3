﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Transactions;
using System.Data.SqlClient;
using System;
using Capstone.Web.DAO;
using Dapper;
using Capstone.Web.Models;
using System.Collections.Generic;
using System.Linq;

namespace Capstone.Web.Tests.DAO
{
    [TestClass]
    public class ParkSqlDAOTests
    {
        private string connString = Helper.CONNSTRING;
        private TransactionScope scope;
        ParkSqlDAO parkDAO;

        private List<Park> parks;
        private int prevNumParks;
        private Park testPark = new Park()
        {
            ParkCode = "test",
            ParkName = "test",
            State = "pa",
            Acreage = 1,
            ElevationInFeet = 1,
            MilesOfTrail = 1,
            NumberOfCampsites = 1,
            Climate = "test",
            YearFounded = 1,
            AnnualVisitorCount = 1,
            InspirationalQuote = "test",
            InspirationalQuoteSource = "test",
            ParkDescription = "test",
            EntryFee = 1,
            NumberOfAnimalSpecies = 1
        };

        [TestInitialize]
        public void Initialize()
        {
            parkDAO = new ParkSqlDAO(connString);
            scope = new TransactionScope();
            try
            {
                using (SqlConnection conn = new SqlConnection(connString))
                {
                    conn.Open();
                    string cmd = "select * from park";
                    parks = conn.Query<Park>(cmd).ToList();
                    prevNumParks = parks.Count;
                    conn.Execute("insert into park values ('zzztest', 'zzztest', 'pa', 1, 1, 1, 1, 'test', 1, 1, 'test', 'test', 'test', 1, 1)");
                }
            } catch (SqlException e)
            {
                throw;
            }
        }

        [TestCleanup]
        public void Cleanup()
        {
            scope.Dispose();
        }


        [TestMethod]
        public void GetAllParksTest()
        {
            parks = parkDAO.GetAllParks();
            Assert.IsNotNull(parks);
            CollectionAssert.AllItemsAreNotNull(parks);
            Assert.AreEqual(prevNumParks + 1, parks.Count);
            Assert.AreEqual("zzztest", parks[parks.Count - 1].ParkName);
        }

        [TestMethod]
        public void GetOneParkTest()
        {
            var park = parkDAO.GetOnePark("zzztest");
            Assert.IsNotNull(park);
            Assert.AreEqual("zzztest", park.ParkCode);
        }

        [TestMethod]
        public void GetOneParkNullTest()
        {
            Assert.IsNull(parkDAO.GetOnePark("nosuch"));
        }

        [TestMethod]
        public void GetAllParksEmptyTest()
        {
            try
            {
                using(var conn = new SqlConnection(connString))
                {
                    conn.Open();
                    conn.Execute("delete from survey_result"); //this should be within the trans scope from init and clenaup
                    conn.Execute("delete from weather"); //this should be within the trans scope from init and clenaup
                    conn.Execute("delete from park"); //this should be within the trans scope from init and clenaup
                }
            } catch (SqlException e)
            {
                throw;
            }
            Assert.AreEqual(0, parkDAO.GetAllParks().Count);
        }

    }
}
