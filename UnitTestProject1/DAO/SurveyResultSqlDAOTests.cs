﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Transactions;
using System.Data.SqlClient;
using System;
using Capstone.Web.DAO;
using Dapper;
using Capstone.Web.Models;
using System.Collections.Generic;
using System.Linq;

namespace Capstone.Web.Tests.DAO
{
    [TestClass]
    public class SurveyResultSqlDAOTests
    {
        private string connString = Helper.CONNSTRING;
        private TransactionScope scope;
        private SurveyResultSqlDAO surveyDAO;

        [TestInitialize]
        public void Initialize()
        {
            surveyDAO = new SurveyResultSqlDAO(connString);

            scope = new TransactionScope();
            try
            {
                using (var conn = new SqlConnection(connString))
                {
                    conn.Open();
                    conn.Execute("insert into park values ('zzztest', 'zzztest', 'pa', 1, 1, 1, 1, 'test', 1, 1, 'test', 'test', 'test', 1, 1)");
                    conn.Execute("insert into park values ('yyytest', 'yyytest', 'pa', 1, 1, 1, 1, 'test', 1, 1, 'test', 'test', 'test', 1, 1)");
                    for (int i = 0; i < 100; i++)
                    {
                    conn.Execute("insert into survey_result values ('zzztest', 'test@email.com', 'pa', 'lazy')");
                    }
                    conn.Execute("insert into survey_result values ('yyytest', 'test@email.com', 'pa', 'lazy')");
                    conn.Execute("insert into survey_result values ('yyytest', 'test@email.com', 'pa', 'lazy')");
                }
            } catch (SqlException e)
            {
                throw;
            }
        }

        [TestCleanup]
        public void Cleanup()
        {
            scope.Dispose();
        }

        [TestMethod]
        public void GetTopParkTest()
        {
            var parks = surveyDAO.GetLeadingParks();
            Assert.IsNotNull(parks);
            CollectionAssert.AllItemsAreNotNull(parks);
            Assert.AreEqual("zzztest", parks.FirstOrDefault().ParkCode);
        }

        [TestMethod]
        public void GetTopParksEmptyTest()
        {
            try
            {
                using (var conn = new SqlConnection(connString))
                {
                    conn.Open();
                    conn.Execute("delete from survey_result"); //this should be within the trans scope from init and clenaup
                    conn.Execute("delete from weather"); //this should be within the trans scope from init and clenaup
                    conn.Execute("delete from park"); //this should be within the trans scope from init and clenaup
                }
            }
            catch (SqlException e)
            {
                throw;
            }
            Assert.AreEqual(0, surveyDAO.GetLeadingParks().Count);

        }

        [TestMethod]
        public void InsertTest()
        {
            Assert.IsTrue(surveyDAO.InsertSurvey(new SurveyResult { ParkCode = "zzztest", EmailAddress = "jeff@ruby.net", State = "PA", ActivityLevel = "whatever" }));
        }

        [TestMethod]
        public void InsertFailTest()
        {
            Assert.IsFalse(surveyDAO.InsertSurvey(new SurveyResult { ParkCode = "nofktest", EmailAddress = "jeff@ruby.net", State = "PA", ActivityLevel = "whatever" }));
            Assert.IsFalse(surveyDAO.InsertSurvey(new SurveyResult { ParkCode = "too long park code test", EmailAddress = "jeff@ruby.net", State = "PA", ActivityLevel = "whatever" }));
        }
    }
}
