﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Capstone.Web.Tests
{
    public static class Helper
    {
        // Change this value to whatever port your VS is using.
        // Also, load your project in another window before running tests and launch it. 
        // Looke at the url, and update the port below.
        // The web server must be running for the tests to work.
        public static readonly string URL = "http://localhost:60349/";

        public static readonly string CONNSTRING = @"Data Source=.\SQLEXPRESS;Initial Catalog=NPGeek;Integrated Security=true";
    }
}
