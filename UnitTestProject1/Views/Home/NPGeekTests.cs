﻿using Capstone.Web.Models;
using Capstone.Web.Tests.Views.Home;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.IO;
using System.Reflection;

namespace Capstone.Web.Tests.Views
{
    [TestClass]
    public class NPGeekTests
    {
        private static IWebDriver driver;
        private IndexPO indexPage;

        [TestInitialize]
        public void Initialize()
        {
            driver = new ChromeDriver(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));
            driver.Navigate().GoToUrl(Helper.URL);
            indexPage = new IndexPO(driver);
        }

        [TestCleanup]
        public void Cleanup()
        {
            driver.Close();
        }

        [TestMethod]
        public void IndexPageReqdElements()
        {
            Assert.IsNotNull(driver.FindElement(By.Id("HomeIndexParkList")));
            Assert.IsNotNull(driver.FindElement(By.CssSelector(".HomeIndexParkTiles .HomeIndexParkImage")));
            Assert.IsNotNull(driver.FindElement(By.CssSelector(".HomeIndexParkTiles .HomeIndexParkTileNameLocation")));
            Assert.IsNotNull(driver.FindElement(By.CssSelector(".HomeIndexParkTiles .HomeIndexParkTileDescription")));
        }

        [TestMethod]
        public void DetailPageReqdElements()
        {
            var detailPage = indexPage.ClickOnNthPark(1);
            Assert.IsNotNull(driver.FindElement(By.Id("HomeDetailImage")));
            Assert.IsNotNull(driver.FindElement(By.Id("HomeDetailParkNameStateYearFounded")));
            Assert.IsNotNull(driver.FindElement(By.Id("HomeDetailInspirationalInfo")));
            Assert.IsNotNull(driver.FindElement(By.Id("HomeDetailParkDescription")));
            Assert.IsNotNull(driver.FindElement(By.Id("HomeDetailMiscInfo")));
            Assert.IsNotNull(driver.FindElement(By.Id("HomeDetailTemperaturePref")));
        }

        [TestMethod]
        public void IndexParkListAlphabetized()
        {
            var alpha = true;
            var previousName = "";
            foreach (var name in indexPage.ParkNameList)
            {
                if (String.Compare(previousName, name) > 0)
                {
                    alpha = false;
                    break;
                }
                previousName = name;
            }
            Assert.IsTrue(alpha);
        }

        [TestMethod]
        public void CanClickThruToDetail()
        {
            string firstParkName = indexPage.GetNthParkName(1);
            Assert.AreEqual(firstParkName, indexPage.ClickOnNthPark(1).ParkName);
        }

        [TestMethod]
        public void SurveyClientSideValidation()
        {
            Assert.IsTrue(indexPage.GoToSurvey().HasClientSideValidation);
        }

        [TestMethod]
        public void SurveyTestHappy()
        {
            Assert.IsTrue(indexPage.GoToSurvey()
                .ChoosePark("CVNP")
                .EnterEmail("test@email.com")
                .ChooseState("PA")
                .ChooseActLevel("Couch Potato")
                .Submit()
                .HasConfirmation);
        }

        [TestMethod]
        public void SurveyTestBadEmail()
        {
            Assert.IsFalse(indexPage.GoToSurvey()
                .ChoosePark("CVNP")
                .EnterEmail("te")
                .ChooseState("PA")
                .ChooseActLevel("Couch Potato")
                .Submit()
                .HasConfirmation);
        }

        [TestMethod]
        public void SurveyTestBlankForm()
        {
            Assert.IsFalse(indexPage.GoToSurvey()
                .Submit()
                .HasConfirmation);
        }

        //[TestMethod]
        //public void SurveyTestBadParkCode()
        //{
        //    Assert.IsTrue(indexPage.GoToSurvey()
        //        .ChoosePark("zzz")
        //        .EnterEmail("test@email.com")
        //        .ChooseState("PA")
        //        .ChooseActLevel("Couch Potato")
        //        .Submit()
        //        .HasConfirmation);
        //}

        [TestMethod]
        public void SurveyResultsOrdered()
        {
            var ordered = true;
            ParkWithVotes previousPark = new ParkWithVotes()
            {
                ParkName = "",
                Votes = 0
            };
            SurveyResultPO resultPage = indexPage.GoToSurvey()
                .ChoosePark("CVNP")
                .EnterEmail("test@email.com")
                .ChooseState("PA")
                .ChooseActLevel("Couch Potato")
                .Submit();

            foreach (var park in resultPage.ParkList)
            {
                if (previousPark.Votes - park.Votes >= 0 && String.Compare(previousPark.ParkName, park.ParkName) < 0)
                {
                    ordered = false;
                    break;
                }
                previousPark = park;
            }

            Assert.IsTrue(ordered);
        }

        [TestMethod]
        public void TempPrefTestHappy()
        {
            Assert.AreEqual("C",indexPage.ClickOnNthPark(1).ChengeToC().TempPref);
        }

        [TestMethod]
        public void TempPrefSessionPersist()
        {
            Assert.AreEqual("C",indexPage.ClickOnNthPark(1).ChengeToC().GoHome().ClickOnNthPark(1).TempPref);
        }
    }
}
