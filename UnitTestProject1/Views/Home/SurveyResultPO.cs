﻿using Capstone.Web.Models;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;

namespace Capstone.Web.Tests.Views.Home
{
    public class SurveyResultPO : NPGeekPO
    {
        public SurveyResultPO(IWebDriver driver) : base(driver) { }

        public bool HasConfirmation
        {
            get
            {
                try
                {
                    return driver.FindElement(By.Id("confirmation")) != null;
                }
                catch (NoSuchElementException)
                {
                    return false;
                }
            }
        }

        //use css 1 based index
        public ParkWithVotes GetNthPark(int n)
        {
            return new ParkWithVotes()
            {
                ParkName = driver.FindElement(By.CssSelector
                    ($"#SurveyResultParkList > div:nth-child({n}) > span.SurveyResultsParkName > a > p")).Text,
                Votes = Convert.ToInt32(driver.FindElement(By.CssSelector
                    ($"#SurveyResultParkList > div:nth-child({n}) > span.DailyVotes > p:nth-child(2)")).Text)
            };
        }

        public IList<ParkWithVotes> ParkList
        {
            get
            {
                var parks = new List<ParkWithVotes>();
                var parkElements = driver.FindElements(By.Id("SurveyResultParkList"));
                for (int n = 1; n <= parkElements.Count; n++)
                    parks.Add(GetNthPark(n));
                return parks;
            }
        }
    }
}
