﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;

namespace Capstone.Web.Tests.Views.Home
{
    public class NPGeekPO
    {
        protected IWebDriver driver;
        protected NPGeekPO(IWebDriver driver)
        {
            this.driver = driver;
        }

        public IndexPO GoHome()
        {
            IWebElement surveyLink = driver.FindElement(By.CssSelector("body > nav.navbar.navbar-inverse.navbar-fixed-top > div > div.navbar-collapse.collapse > ul > li:nth-child(1) > a"));
            surveyLink.Click();
            return new IndexPO(driver);
        }


        public SurveyPO GoToSurvey()
        {
            IWebElement surveyLink = driver.FindElement(By.CssSelector("body > nav.navbar.navbar-inverse.navbar-fixed-top > div > div.navbar-collapse.collapse > ul > li:nth-child(2) > a"));
            surveyLink.Click();
            return new SurveyPO(driver);
        }

    }
}
