﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;

namespace Capstone.Web.Tests.Views.Home
{
    public class IndexPO : NPGeekPO
    {
        public IndexPO(IWebDriver driver) : base(driver) { }

        public DetailPO ClickOnNthPark(int n)
        {
            IWebElement parkLink = driver.FindElement(By.CssSelector($"#HomeIndexParkList > div:nth-child({n}) > a"));
            parkLink.Click();
            return new DetailPO(driver);
        }

        //used css 1 based index
        public string GetNthParkName(int n)
        {
            return driver.FindElement(By.CssSelector
                ($"#HomeIndexParkList > div:nth-child({n}) > div > div.HomeIndexParkTileNameLocation > a")).Text;
        }

        public List<string> ParkNameList
        {
            get
            {
                var names = new List<string>();
                var parks = driver.FindElements(By.Id("HomeIndexParkList"));
                for (int n = 1; n <= parks.Count; n++)
                    names.Add(GetNthParkName(n));
                return names;
            }
        }
    }
}
