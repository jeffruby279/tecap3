﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System.Collections.Generic;

namespace Capstone.Web.Tests.Views.Home
{
    public class SurveyPO : NPGeekPO
    {
        public SurveyPO(IWebDriver driver) : base(driver) { }

        public bool HasClientSideValidation
        {
            get
            {
                return driver.FindElement(By.Id("ParkCode")).GetAttribute("data-val") == "true" 
                    && driver.FindElement(By.Id("EmailAddress")).GetAttribute("data-val") == "true"
                    && driver.FindElement(By.Id("State")).GetAttribute("data-val") == "true"
                    && driver.FindElement(By.Id("ActivityLevel")).GetAttribute("data-val") == "true";
            }
        }

        public SurveyPO ChoosePark(string parkCode)
        {
            SelectElement field = new SelectElement(driver.FindElement(By.Id("ParkCode")));
            field.SelectByValue(parkCode);
            return this;
        }

        public SurveyPO EnterEmail(string email)
        {
            IWebElement field = driver.FindElement(By.Id("EmailAddress"));
            field.SendKeys(email);
            return this;
        }

        public SurveyPO ChooseState(string stateCode)
        {
            SelectElement field = new SelectElement(driver.FindElement(By.Id("State")));
            field.SelectByValue(stateCode);
            return this;
        }

        public SurveyPO ChooseActLevel(string activityLevel)
        {
            IWebElement field = driver.FindElement(By.CssSelector($"#ActivityLevel[value='{activityLevel}']"));
            field.Click();
            return this;
        }

        public SurveyResultPO Submit()
        {
            IWebElement field = driver.FindElement(By.CssSelector("body > div.container.body-content > form > input[type='submit']:nth-child(5)"));
            field.Click();
            return new SurveyResultPO(driver);
        }
    }
}