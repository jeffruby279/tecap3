﻿using OpenQA.Selenium;

namespace Capstone.Web.Tests.Views.Home
{
    public class DetailPO : NPGeekPO
    {
        public DetailPO(IWebDriver driver) : base(driver) { }

        public DetailPO ChengeToC()
        {
            IWebElement button = driver.FindElement(By.CssSelector("#HomeDetailTempButtons > a:nth-child(2)"));
            button.Click();
            return new DetailPO(driver);
        }

        public DetailPO ChengeToF()
        {
            IWebElement button = driver.FindElement(By.CssSelector("#HomeDetailTempButtons > a:nth-child(1)"));
            button.Click();
            return new DetailPO(driver);
        }

        public string TempPref => driver.FindElement(By.Id("temppref")).Text;

        public string ParkName => driver.FindElement(By.CssSelector("#HomeDetailParkNameStateYearFounded > p:nth-child(1)")).Text;
    }
}
